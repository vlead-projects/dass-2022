# DASS-2022


## Introduction 
  This document serves as a dashboard of the 2022 - DASS projects.
 
## Name of the Company
  Virtual Labs Engineering and Architecture Division

## Primary Work Location
  Work From Home (online)

## Email
  engg@vlabs.ac.in; dass-2022@vlabs.ac.in

## Projects Proposed
  - Project-1 - Re-engineering of [VLSI](http://cse14-iiith.vlabs.ac.in/) Virtual Lab using Open Web Technologies

  - Project-2 - Re-engineering of [Digital Logic Design](http://cse15-iiith.vlabs.ac.in/) Virtual Lab using Open Web Technologies

  - Project-3 - Re-engineering of [Computer Graphics](http://cse18-iiith.vlabs.ac.in/) Virtual Lab using Open Web Technologies

  - Project-4 - Re-engineering of [Basic Structral Analysis](http://bsa-iiith.vlabs.ac.in/) Virtual Lab using Open Web Technologies


  ## Project Tasks and Outcome
  
   All the above projects will involve the following tasks 

  ### Tasks   
   1. Study the lab (experiment) and understand the interface.
   
   2. Identify the re-engineering requirements.  

  ### Outcome 
  
  A document with recommendations on design and open sourced libraries.

  ### Tasks
   3. Complete re-engineering of all the experiments using Open Web Technologies. Follow the [link](https://github.com/virtual-labs/ph3-exp-dev-process/tree/main/best-practices) for best practices and colour themes to be followed during development.

   4. Test and fix bugs: Use Bug reporting tool to log and record bug lifecycle. 

   5. Meet Performance Requirement - Use Performance Tool to measure and improve the performance to meet the following criteria for each page - 

          a. Page size < 5MB
   
          b. Page load time < 1.5 sec over fast 3G network 
   
  ### Outcome
  
  Experiment should run on Firefox, Chrome, Safari and Edge browser with a **responsive simulation**. 

  
  ## Sample Experiment 

  - **Lab Name : **Soil Mechanics Lab**

  - **Exp Name : **Water Content**

  - **Source Code : **[Link](https://github.com/virtual-labs/exp-water-content-iiith)**

  - **Hosted URL : **[Link](https://smfe-iiith.vlabs.ac.in/exp/water-content/)**


## Number of People
  Four students per project. 
## Technology
  Javascript, CSS and HTML
## Team Members and Contact Information  

  TA - Shlok Pandey - shlok.pandey@research.iiit.ac.in



| SNo  |Team ID   |Members in the team   | Email address/Phone Number  | Github Handle/Gitlab Handle  | Lab Name/Hosted Link | Assigned Experiment names/repo links|
|---|---|---|---|---|---|---|
| 1.| Project1  | 1. Venneti Sri Satya Vinay<br>2. Rajarshi Ray<br>3. Shubhankar Kamthankar<br>4. Katha Rohan Reddy  |   |   |[VLSI](https://cse14-iiith.vlabs.ac.in/)   |1. [Schematic Design Of Transistor Level Inverter](https://github.com/virtual-labs/exp-transistor-level-inverter-iiith)<br>2. [Schematic Design Of Transistor Level NAND & NOR Gate](https://github.com/virtual-labs/exp-transistor-level-nand-iiith) <br>3. [Schematic Design Of Transistor Level XOR & XNOR Gate](https://github.com/virtual-labs/exp-transistor-level-xor-iiith) <br>4. [Schematic Design Of Pass Transistor Logic & Multiplexer](https://github.com/virtual-labs/exp-pass-transistor-logic-iiith) <br>5. [Delay Estimation In Chain Of Inverters](https://github.com/virtual-labs/exp-chain-of-inverters-iiith) <br>6. [Schematic Design Of D-Latch and D-Flip Flop]( https://github.com/virtual-labs/exp-d-latch-and-d-flip-flop-iiith) <br>7. [Spice Code Platform](https://github.com/virtual-labs/exp-spice-code-platform-iiith) <br>8. [Design Of D-Flip Flop Using Verilog](https://github.com/virtual-labs/exp-d-flip-flop-verilog-iiith) <br>9. [Design Of Digital Circuits Using Verilog](https://github.com/virtual-labs/exp-digital-circuits-verilog-iiith)<br>10. [Layout Design](https://github.com/virtual-labs/exp-layout-design-iiith)|
| 2.| Project2  | 1. Aditya Kumar<br>2. Shreyash Jain<br>3. Mayank Bhardwaj<br>4. Urvish Pujara  |   |   |[Digital Logic Design](https://cse15-iiith.vlabs.ac.in/)| 1. [Adder Circuit](https://github.com/virtual-labs/exp-adder-circuit-iiith) <br>2. [Multiplexer](https://github.com/virtual-labs/exp-multiplexer-iiith) <br>3. [Decoder with 7-Segment Display](https://github.com/virtual-labs/exp-7-segment-display-iiith) <br>4. [ALU with function](https://github.com/virtual-labs/exp-alu-iiith)<br>5. [Comparator](https://github.com/virtual-labs/exp-comparator-iiith) <br>6. [Latch and Flip Flops](https://github.com/virtual-labs/exp-latch-and-flip-flops-iiith)<br>7. [Registers](https://github.com/virtual-labs/exp-registers-iiith)<br>8. [Counters](https://github.com/virtual-labs/exp-counters-iiith)<br>9. [Multipliers](https://github.com/virtual-labs/exp-multipliers-iiith)<br>10. [State Diagrams](https://github.com/virtual-labs/exp-state-diagrams-iiith)|
| 3.| Project3  | 1. Polakampalli Sai Namrath<br>2. Meka Sai Mukund<br>3. Yug Dedhia<br>4. Padala Sudheer Reddy  |   |   |[Computer Graphics](https://cse18-iiith.vlabs.ac.in/)   | 1.[Points and Co-ordinate Systems](https://github.com/virtual-labs/exp-coordinate-systems-iiith)<br>2. [Transformations: Translation](https://github.com/virtual-labs/exp-transformations-translation-iiith)<br>3. [Transformations: Rotation](https://github.com/virtual-labs/exp-transformations-rotation-iiith)<br>4. [Transformations: Scaling](https://github.com/virtual-labs/exp-transformations-scaling-iiith)<br>5. [Hierarchical Transformations: 2D Demo](https://github.com/virtual-labs/exp-2d-demo-iiith)<br>6. [Hierarchical Transformations: 3D Articulated Arm](https://github.com/virtual-labs/exp-3d-articulated-arm-iiith)<br>7. [Projections and Cameras](https://github.com/virtual-labs/exp-projections-and-cameras-iiith)<br>8. [Clipping: Line](https://github.com/virtual-labs/exp-clipping-line-iiith)<br>9. [Clipping: Polygon](https://github.com/virtual-labs/exp-clipping-polygon-iiith)<br>10. [Rasterization: Line](https://github.com/virtual-labs/exp-rasterization-line-iiith)<br>11. [Rasterization: Polygon](https://github.com/virtual-labs/exp-rasterization-polygon-iiith)|
| 4.| Project4  | 1. Balamma Boya<br>2. Sravanthi Modepu<br>3. Mrudhvika Damaraju  |   |   |[Basic Structral Analysis](https://bsa-iiith.vlabs.ac.in/)   |1. [Single Span Beams Experiment](https://github.com/virtual-labs/exp-single-span-beams-iiith) <br>2. [Continuous Beams Experiment](https://github.com/virtual-labs/exp-continuous-beams-iiith)<br>3. [Column Analysis Experiment](https://github.com/virtual-labs/exp-column-analysis-iiith)<br>4. [Portal Frames Experiment](https://github.com/virtual-labs/exp-portal-frames-iiith)<br>5. [Plates Experiment](https://github.com/virtual-labs/exp-plates-iiith)<br>6. [Rigid Joints Experiment](https://github.com/virtual-labs/exp-rigid-joints-iiith)<br>7. [Arches Experiment](https://github.com/virtual-labs/exp-arches-iiith)<br>8. [Trusses Experiment](https://github.com/virtual-labs/exp-trusses-iiith)<br>9. [Retaining Walls Experiment](https://github.com/virtual-labs/exp-retaining-walls-iiith)<br>10. [Plastic Hinge Experiment](https://github.com/virtual-labs/exp-plastic-hinge-iiith)|

  
## Working Guidelines 

### Development Process 

Step 1 : Choose one exp from the assigned list and send a mail to systems@vlabs.ac.in with your Github handle and associated email id requesting to be added to the repository.  

Step 2 : Clone the repository.

Step 3 : Populate the dev branches of the repository with the source code of the experiments and unit test the experiments locally. Do not delete gh-pages branch. This is required for automatically deploying the experiment along with UI on GitHub pages for testing the complete experiment .Also do not delete .github directory and the LICENSE file.

Step 4 : Merge the fully tested dev branch to testing branch. This will automatically deploy the experiment along with UI on GitHub pages for testing the complete experiment. You will receive an email about the success/failure of the deployment on the email id associated with the github handle provided in Step 1. 

Step 5 : Set up a demo with the Virtual Labs Team and merge the code with **main branch** only after getting approval from them. 


### Realization in terms of Milestones
  Follow [agile](https://en.wikipedia.org/wiki/Agile_software_development) software development and use [scrum](https://www.scrumalliance.org/why-scrum)  methodology to incrementally working software.  Every project has a product backlog.  In each sprint, items from the product backlog are picked to be realized in a milestone.  Each item from a product backlog translates to one or more tasks and each task is tracked as an issue on
  github.  A milestone is a collection of issues.  It is upto the mentor and the team to choose either one or two week sprints.

  Each release is working software and a release determines the achievement of a milestone.  A project is realized as a series of milestones.

  This planning will be part of the master repo of respective project repos.
  
### Communication 
  Every discussion about the project will be through issues.  Mails are not used for discussion.
  Any informal communication will be through Slack - dass-2022 channel.

### Total person hours  ( As committed by the course coordinator) 
   Design and Analysis of Software Systems (DASS, formerly SSAD) course for the 2nd year students to work on. As you know, the project component carries 40% weightage for the course, and it would be great if the students would get an opportunity to work with you and the team as their
   clients.

   Each student expected to devote 10 hours per week for their projects. It is not 10 hours per team, rather 10 hours per student. Just confirmed it with Prof. Ramesh. There are a total of 12 productive weeks that the student must work for the project. (3 in January, 3 in February, 4 in March and 2 in April). This is excluding the mid-semester week, as that wouldn't be a productive one. There would be 4 members per team, so each team devotes
   40 hours per week and 480 hours in total for the project.

   Since these project are meant to simulate a proper industrial project, they would have to sincerely meet with the clients regularly, document everything, keep track of their status on git and most importantly complete the project and get a good client feedback, else they'd get a bad grade! Kindly requesting you to open up some Virtual Labs projects for the students to work on.
   
### Things done at the weekly meeting

  1. Every meeting starts with a presentation of the code documents and a demonstration.

  2. The pull/merge request is reviewed at the meeting.

  3. Task planning for the next week is discussed.  The students take the meeting notes.  The meeting notes has
     the below structure.  From the meeting, action items are thrashed out and are listed.  The mom file is also part of this repository.  Sample mom is placed under [meetings](https://gitlab.com/vlead-projects/dass-2020/tree/master/%20meeting) directory. You may also refer to the [mom](https://gitlab.com/vlead-projects/experiments/2018-ssad/index/blob/master/src/meetings/2018-08-31-mom.org)
     which captures the mom of all the teams. 

   
     ** Agenda

     ** Discussion Points

     ** Action

     |   |   |   |   |   
     |---|---|---|---| 
     | Item  |  Issue | Artifact   |Status    |   
     | Implement filter|Link to| link to file|In progress|issue |in the repo 


### Work logs
  The work logs of each student will need to be pushed to the repository shared by the TA/Course Coordinator. The work-logs will consider for Grading. 

### Team Meetings
  Team meetings will be held every Wednesday from 7:00 PM till 8:00 PM with a slotted time of 15 min per team.
  


