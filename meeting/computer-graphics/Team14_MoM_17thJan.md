# Minutes of Meeting
 
### Date : 17-01-22
### Time : 17:00
### Topic: First Client Meet
### Attendees: 
1. Priya                        - Virtual Labs Product Owner
2. Raj Agnihotri                - Technology Head
3. VJS Pranavasri               - Research Assistant
4. Shlok Pandey                 - mentor TA
5. Meka Sai Mukund              - Student
6. Polakampalli Sai Namrath     - Student
7. Padala Sudheer Reddy         - Student
8. Yug Dedhia                   - Student

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | vlabs - An experiments-based platform for interactive learning  (Sponsored by MoE, Govt of India) .  | - | - 
I | vlabs structure - The website contains content from various higher education disciplines, which are explained simply and in a fun way via simulations and experiments. Each lab is further divided into learning units and tasks. Every experiment has various sections such as Aim, Theory,pretest, Procedure, Simulation, Post test, References and Feedback. | - | - 
I |Aim of the project - To improve the portability and usability of the simulations in the computer graphics section by rewriting them using open standard web technologies. There are a total of 11 experiments in this section. | - | -
D | Scheduled time for the meet - Meet was scheduled every Wednesday 5-6 PM to discuss and review our progress | - | -
I | we were given an overview on templates of experiments | - | -
T | Tasks - Join Slack Platform and GitLab with our respective accounts.Study the lab and understand the interface.Identify the reengineering requirements and read the development process and best practices documentations.| - | 21-1-2022
T | Languages and Softwares to be used - html,css and javascript would be the primary languages for coding part of the project. We have to choose a library that has a good support base, which lot of users use and which gives a fast response time(low overhead). Vlabs has a custom library which is based on bulma css which we are recommended to use as well as animejs. | - | -
