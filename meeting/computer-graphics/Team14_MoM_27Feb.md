# Minutes of Meeting
 
### Date : 27-02-22
### Time : 17:00
### Topic: First Client Meet
### Attendees: 
1. Priya                        - Virtual Labs Product Owner
2. Raj Agnihotri                - Technology Head
3. VJS Pranavasri               - Research Assistant
4. Shlok Pandey                 - mentor TA
5. Meka Sai Mukund              - Student
6. Polakampalli Sai Namrath     - Student
7. Padala Sudheer Reddy         - Student
8. Yug Dedhia                   - Student

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | Progress Made last week | - | - 
I | Showed the code written so far for JS and HTML/CSS | - | - 
I | Suggested changes in the naming conventions used | - | -
I | Discussion regarding clarity in the experiments | - | -
I | Considered requesting for an SME if needed| - | -
