# Minutes of Meeting
 
### Date : 02-02-2022
### Time : 17:00
### Topic: Third Client Meet
### Attendees: 
1. Priya                        - Virtual Labs Product Owner
2. Raj Agnihotri                - Technology Head
3. VJS Pranavasri               - Research Assistant
4. Shlok Pandey                 - Mentor TA
5. Meka Sai Mukund              - Student
6. Polakampalli Sai Namrath     - Student
7. Padala Sudheer Reddy         - Student
8. Yug Dedhia                   - Student

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | Discussed about SRS and Project docs  | - | - 
D | Libraries for the project - Decided to submit a document regarding Library Choice  | Raj Agnihotri | 05-02-2022 
I | Progress made in the last week by Team 14. | - | -
I | Decided to meet with the developers regarding clarity on features in various experiments | - | -
T | Decided on submitting a High-Overview Project Plan by next Client Meet | - | 
I | Requested to meet with Support Team to help run some of the experiments | - | 
