# Minutes of Meeting
 
### Date : 11-03-22
### Time : 17:00
### Topic: First Client Meet
### Attendees: 
1. Priya                        - Virtual Labs Product Owner
2. Raj Agnihotri                - Technology Head
3. VJS Pranavasri               - Research Assistant
4. Shlok Pandey                 - mentor TA
5. Meka Sai Mukund              - Student
6. Polakampalli Sai Namrath     - Student
7. Padala Sudheer Reddy         - Student
8. Yug Dedhia                   - Student

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | Progress Made last week - Prettifying the code .  | - | - 
I | Progress Made Last week - Experiment 2 Simulation | - | - 
I | Progress made Last week - Camera feature | - | -
I | Discussion regarding SME for the experiment | - | -
T | Decided to work on the slider to re-adjust itself in a different way after contacting SME | - | -
