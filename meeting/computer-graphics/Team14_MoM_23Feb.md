# Minutes of Meeting
 
### Date : 23-02-22
### Time : 17:00
### Topic: First Client Meet
### Attendees: 
1. Priya                        - Virtual Labs Product Owner
2. Raj Agnihotri                - Technology Head
3. VJS Pranavasri               - Research Assistant
4. Shlok Pandey                 - mentor TA
5. Meka Sai Mukund              - Student
6. Polakampalli Sai Namrath     - Student
7. Padala Sudheer Reddy         - Student
8. Yug Dedhia                   - Student

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | Progress Made last week | - | - 
I | Finished Layout for experiment-1 | - | - 
I | Discussion on the layout and suggested changes | - | -
I | Submitted DesignDoc | - | -
I | Showed demo of currently done simulation in JS | - | -
