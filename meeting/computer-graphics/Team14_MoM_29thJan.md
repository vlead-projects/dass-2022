# Minutes of Meeting
 
### Date : 29-01-2022
### Time : 15:00
### Topic: Second Client Meet
### Attendees: 
1. Priya                        - Virtual Labs Product Owner
2. Raj Agnihotri                - Technology Head
3. VJS Pranavasri               - Research Assistant
4. Shlok Pandey                 - Mentor TA
5. Meka Sai Mukund              - Student
6. Polakampalli Sai Namrath     - Student
7. Padala Sudheer Reddy         - Student
8. Yug Dedhia                   - Student

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | Discussed about issues regarding VirtualBox installation.  | - | - 
I | Libraries for the project - Mentioned to try and reduce the number of external libraries used and try to use ones with larger communities for support | - | - 
I | Presented the progress made in the last week by Team 14. | - | -
D | Document the list of libraries we'd like to use and finalise it with the team | - | -
I |Discussed regarding usability of various libraries | - | -
T | Tasks - Try to run simulations and discuss on slack if need be.| - | 02-02-2022
T | Try to go through 1st experiment, make a simulation and clarify queries | - | -
