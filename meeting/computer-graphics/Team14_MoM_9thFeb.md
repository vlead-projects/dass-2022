# Minutes of Meeting
 
### Date : 09-02-2022
### Time : 17:00
### Topic: Fourth Client Meet
### Attendees: 
1. Priya                        - Virtual Labs Product Owner
2. Raj Agnihotri                - Technology Head
3. VJS Pranavasri               - Research Assistant
4. Shlok Pandey                 - Mentor TA
5. Meka Sai Mukund              - Student
6. Polakampalli Sai Namrath     - Student
7. Padala Sudheer Reddy         - Student
8. Yug Dedhia                   - Student

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | Gave a run-down on progress made, and plans for the week coming  | - | - 
I | Gave a Demo on how we're expecting the mouse-click features to work  | - | -
I | Was suggested to keep Mobile-responsiveness in mind due to large user base | - | -
I | Was suggested to keep mid-semester exams in mind in our project planning | - | -
I | Was suggested to use slack to keep client updated regarding UI and outline, for suggestions and ideas | - | -
I | Was suggested to use docs to document progress and update it regularly to help make it easier | - | -
