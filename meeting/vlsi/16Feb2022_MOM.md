# Minutes of Meeting
 
### Date : 16-02-2022
### Time : 17:00
### Topic: Fifth Client Meet
### Attendees: 

**Clients:**

- Raj Agnihotri
- Priya
- VJ Pranavasri

**TA:**

- Shlok Pandey

**Attendees from our team:**

- Venneti Sri Satya Vinay
- Katha Rohan Reddy
- Shubhankar Kamtankar
- Rajarshi Ray

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | We showed them a demo of connecting components by wires, drag & drop of components etc. which we made using jsPlumb library | - | -
I | But we were told that the library gets approved only after a review | - | -
I | The library got approved on 17 February 2022 | - | -
T | Before that we need to update about jsPlumb in library recommendations documnet | Entire team | Entire team
I | In the demo we showed we were suggested that the whole simulation could be made such that the user doesn't needs to scroll | - | -
I | We showed the client team SVGs we designed and they got approved. | - | -
I | We were told that VJ Pranavasri would be reviewing the code we push to github repo | - | -
I | We need to push code to the `dev` branch as soon as we write something reviewable enough so that the review process gets easy | - | -
D | We decided to complete atleast two experiments by next week including the responsive part and the graph part | - | -
I | We requested a meeting with SME regarding some conceptual doubts in the experiments | - | -
I | We were asked to have more communication on slack and inform them before we start doing anything | - | -
