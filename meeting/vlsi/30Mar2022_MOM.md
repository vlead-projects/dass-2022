# Minutes of Meeting
 
### Date : 30-03-2022
### Time : 17:00
### Topic: Ninth Client Meet
### Attendees: 

**Clients:**

- Raj Agnihotri
- Priya
- VJ Pranavasri

**TA:**

- Shlok Pandey

**Attendees from our team:**

- Venneti Sri Satya Vinay
- Katha Rohan Reddy
- Shubhankar Kamtankar
- Rajarshi Ray

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
I | We informed the client that we started to do the procedure, pretest, posttest. We completed error handling | - | -
D | We plan to do experiment 10 in the next week | - | -
I | We were asked to do the changes suggested in the code review and send code of the remaining experiments for review as fast as possible | - | -
I | We confirmed whether we have to the demo part and we were asked to do it and in our experiment pratice and the simulation part are the same. | - | -
