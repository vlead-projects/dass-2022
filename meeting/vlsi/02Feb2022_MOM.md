# Minutes of Meeting
 
### Date : 02-02-2022
### Time : 17:00
### Topic: Third Client Meet
### Attendees: 

**Clients:**

- Raj Agnihotri
- Priya

**TA:**

- Shlok Pandey

**Attendees from our team:**

- Venneti Sri Satya Vinay
- Katha Rohan Reddy
- Shubhankar Kamtankar

Type | Description | Owner | Deadline
---- | ---- | ---- | ----
T | Merge all MOMs, documents that we previously commited to a different branch in the repo to the master branch | Entire team | Next Week
T | Change the library recommendations document to doc format and update it in Gitlab | Entire team | Next week
T | Upload the submitted SRS in doc format to the given google drive | Entire team | Next Week
I | We will be given review of the library recommendations document soon | - | -
D | We decided to get started with the first experiment | - | -
D | We decided to make all the circuit components this week | - | -
