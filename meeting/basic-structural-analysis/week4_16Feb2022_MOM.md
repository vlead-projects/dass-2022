# Minutes of the Meeting 

### Date: 16-02-2022
### Time :17:00
### Topic:Fourth Client Meet

## Attendees: DASS 2022 Team 

### Team Members  
    - Balamma
    - Mrudhvika
    - Sravanthi

Type | Description | Owner | Deadline
-----|-------------|-------|---------
T    | UI implementation of  the demo using bulma/virtual-style| Entire Team | Next week 
T    | Enhance the functionality for the Dropdowns and buttons| Entire Team| Next week
T    | Work on animation part of the experiment| Entire Team| Next week
L    | Any problem faced by us needs to be posted on slack to get help| Entire Team | -
L    | Before starting designing something an intimation needs to given to the client via slack and get a confirmation from them| Entire Team| - 
L    | The procedure part in the experiment should be changed according to the simulation| Entire Team| - 
L    | Plans for the next week that we have written in the slides should be very clear|


## Meeting with SME(Bharat)

Attendees: Bharat, Sravanthi, Balamma, Mrudhvika


1. Experiment 7 content has to be update which is same with Experiment 8 Truss  - Bharat will go over the content he will give us the correct one
2. Experiment 8 and Experiment 10 he will go through the content and help us
3. Experiment 1 : We have showed the our experiment which we developing. he explained the each element function and suggested go over the flash ui which is good for users to understand . Regarding graphs he will give formulas which is related to the simulation by tomorrow
4. He said will give quiz questions for all experiments
5. We will have weekly meeting according to his available time 
6. He will provide content where ever required but he will give on paper which is more comfortable to him





