# Minutes of the Meeting 

### Date: 09-02-2022
### Time :17:00
### Topic:Third Client Meet

## Attendees: DASS 2022 Team 

### Team Members  
    - Balamma
    - Mrudhvika
    - Sravanthi

Type | Description | Owner | Deadline
-----|-------------|-------|---------
T    | Write Project SRS document| Entire Team | Next week 
T    | Draw design Layout for the single span beam experiment| Entire Team| Next week
T    | Work on implementing the experiment| Entire Team| Next week
L    | Any problem faced by us needs to be posted on slack to get help| Entire Team | -
L    | Before starting designing something an intimation needs to given to the client via slack and get a confirmation from them| Entire Team| - 
L    | The procedure part in the experiment should be changed according to the simulation| Entire Team| - 
L    | Plans for the next week that we have written in the slides should be very clear|







