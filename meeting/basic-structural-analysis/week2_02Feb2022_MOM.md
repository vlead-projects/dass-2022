# Minutes of the Meeting 

### Date: 02-02-2022
### Time :17:00
### Topic:Second Client Meet

## Attendees: DASS 2022 Team 

### Team Members  
    - Balamma
    - Mrudhvika
    - Sravanthi

Type | Description | Owner | Deadline
-----|-------------|-------|---------
T    |After designing the single spam experiment parts like buttons, sliders, dropdown menu, graphs, diagrams. We need to give  demo in the next meeting by comparing with old and new design| Entire Team | Next week 
T    | Need to make a document comapring different libraries which we have explored in this week and come to conclusion in which library to implement the project and upload the related document on gitlab and post it on slack channel| Entire Team| Next week
L    | Any problem faced by us needs to be posted on slack to get help| Entire Team | -
L    | Before starting designing something an intimation needs to given to the client via slack and get a confirmation from them| Entire Team| - 
L    | The procedure part in the experiment should be changed according to the simulation| Entire Team| - 
L    | Plans for the next week that we have written in the slides should be very clear|






