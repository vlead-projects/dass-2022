# Minutes of Meeting

### Date: 29/01/2022
### Time: 15:00 hrs
### Attendees: Team 2 (Aditya Kumar , Mayank Bhardwaj , Shreyash Jain, Urvish Pujara), Client (Priya,Raj Agnihotri)
### Topic: Second Client Meet

+ **Tasks** : 1) Installation of virtual machine on mac OS and run simulations on all machines successfully.
+ 2) Going through the experiments involved in Digital Logic Design.
+ 3) Doing a dry-run of experiments in our system.
+ 4) Going through the developer documents and best practices and selecting specfic areas of improvement in the experiments.
+ 5) Going through different javascript libraries for animations.

+ **Decisions** : 1) Split our team into group of 2.
+ 2) Make a presentation on weekly basis showing our progress and project related doubts.
+ 3) Drafting documents for libraries we would like to use in our projects.

+ **Information** : 1) To use slack channel for sharing resources and getting better resources .
+ 2) Update sprints on gitlab and update record of meetings and keep a tab on slack channel for updates.