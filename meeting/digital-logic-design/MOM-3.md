# Minutes of Meeting

### Date:2/02/2022
### Time: 17:00 hrs
### Attendees: Team 2 (Mayank Bhardwaj) Client (Priya,Raj Agnihotri)
### Topic: Third Client Meet

+ **Tasks** : 1) To submit the representation doc before the next meet.
+ 2) Going through the experiments involved in Digital Logic Design.
+ 3) Doing a dry-run of experiments in our system.
+ 4) Come up with a general layout of the implementation and clarifying all the design requirements.

+ **Decisions** : 1) Split our team into group of 2.
+ 2) Make a presentation on weekly basis showing our progress and project related doubts.

+ **Information** : 1) To use slack channel for sharing resources and getting better resources .
+ 2) Introduction to the content structure of virtual lab.
+ 3) Update sprints on gitlab and update record of meetings and keep a tab on slack channel for updates.
+ 4) To check functionality and rating of libraries beforing use them.

Note- Due to the unavailibility of 3 team members (Aditya Kumar , Shreyash Jain, Urvish Pujara) , the team was not allowed to present the documentation and was given an alternate meeting to do that instead.

