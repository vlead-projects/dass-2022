# Minutes of Meeting

### Date: 17/01/2022
### Time: 17:00 hrs
### Attendees: Team 2 (Aditya Kumar , Mayank Bhardwaj , Shreyash Jain, Urvish Pujara), Client (Priya,Raj Agnihotri)
### Topic: First Client Meet

+ **Tasks** : 1) Installation of virtual machine for runnning simulations.
+ 2) Going through the experiments involved in Digital Logic Design.
+ 3) Identify the re-engineering requirement.
+ 4) Going through the developer documents and best practices.

+ **Decisions** : 1) Split our team into group of 2.
+ 2) Make a presentation on weekly basis showing our progress and project related doubts.

+ **Information** : 1) Introduction to virtual labs and the activities(such as common deveopement platform,cloud hosting etc.).
+ 2) Introduction to the content structure of virtual lab.
+ 3) Explanation of Experiment lifecycle and demonstration of gitlab .