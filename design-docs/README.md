This directory contains links to documents with recommendations on design and open sourced libraries for each of the DASS project. Please fill the link to Google docs against your respective projects. 


| Project Number | Lab Name | Document Link|
|---|---|---|
| 1.| [VLSI](https://cse14-iiith.vlabs.ac.in/) | 1. [Library recommendations document](https://docs.google.com/document/d/1rZUoRDmJgvmFf8gX7EBmwWJV1vITfQpmmYILgoNQSTs/edit?usp=sharing) <br> 2. [SRS document](https://docs.google.com/document/d/1h4cqzWmQu6XsfBHZNINpDTNazvzyipA-3f1zfM1XjbM/edit?usp=sharing) |
| 2.| [Digital Logic Design](https://cse15-iiith.vlabs.ac.in/) | [Project Library Recommendations](https://docs.google.com/document/d/1mEdxdxDKpO5mlMi8yHRaa6KK1xcLnFDB/edit?usp=sharing&ouid=106203791705403244896&rtpof=true&sd=true) |
| 3.| [Computer Graphics](https://cse18-iiith.vlabs.ac.in/) | [Project Recommendations](https://docs.google.com/document/d/1Rxiwjk22GwB37uQVMuwR-Og4dauFCoYtQUqbRiPbLno/edit?usp=sharing) |
| 4.| [Basic Structural Analysis](https://bsa-iiith.vlabs.ac.in/) | 1. [Library-reccomendation-doc](https://docs.google.com/document/d/1bqzOLuS6IyWhxzrjC4jYpWyDXkI5S-x8/edit) 
 2. [Project-SRS-doc](https://docs.google.com/document/d/1IThTR0nmumGlKyeV9YGPNBeoxZXVkfea/edit?rtpof=true) |
